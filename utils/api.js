import { fetchGraphql as fetch } from "react-tinacms-strapi";

export function getStrapiURL(path = "") {
  return `${process.env.STRAPI_URL || "http://localhost:1337"}${path}`;
}

// Helper to make GET requests to Strapi
export async function fetchAPI(path) {
  const requestUrl = getStrapiURL(path);
  const response = await fetch(requestUrl);
  const data = await response.json();
  return data;
}

export async function fetchGraphql(query, variables) {
  const requestUrl = getStrapiURL();
  const { data } = await fetch(requestUrl, query, variables);

  return data;
}

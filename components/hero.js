import styled from "styled-components";
import { InlineText } from "react-tinacms-inline";
import { getStrapiMedia } from "../utils/media";

export const Hero = ({ heroImage, CTA }) => {
  return (
    <Container image={getStrapiMedia(heroImage)}>
      <h1>
        <InlineText name="Hero.heroTitle" />
      </h1>
      <p>
        <InlineText name="Hero.heroTagline" />
      </p>
      <button href={CTA.ctaLink}>{CTA.ctaText}</button>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 50vh;
  width: 100%;
  background: url(${({ image }) => image});
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;

  h1 {
    font-size: 48px;
  }
`;

import { useCMS } from "tinacms";
import styled from "styled-components";

export default function EditButton() {
  const cms = useCMS();
  return (
    <Button onClick={() => (cms.enabled ? cms.disable() : cms.enable())}>
      {cms.enabled ? "Stop editing" : "Edit this site"}
    </Button>
  );
}

const Button = styled.button`
  position: fixed;
  bottom: 1rem;
  right: 1rem;
  padding: 0.5rem 1.5rem;
  color: #fff;
  background-color: #ec4815;
  border-radius: 3px;
  border: none;
  cursor: pointer;
  transition: background-color 0.23s ease-in-out;

  &:hover {
    background-color: #ce411d;
  }
`;

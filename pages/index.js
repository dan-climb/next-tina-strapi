import Head from "next/head";
import styled from "styled-components";
import { useCMS, useForm, usePlugin } from "tinacms";
import { InlineForm } from "react-tinacms-inline";
import { Hero } from "../components/hero";
import { Product } from "../components/product";
import { gql, useMutation } from "@apollo/client";
import { initializeApollo, addApolloState } from "../utils/apolloClient";
import { cleanValues } from "../utils/functions";

export default function Home({ home: initialHome, preview }) {
  const cms = useCMS();
  const [updateHomePage] = useMutation(updateHomePageMutation);
  const formConfig = {
    id: initialHome.id,
    label: "Home Page",
    initialValues: initialHome,
    onSubmit: async (values) => {
      const clean = cleanValues(values);
      const { data } = await updateHomePage({
        variables: {
          input: {
            data: {
              Hero: {
                ...clean.Hero,
                heroImage: clean.Hero.heroImage.id,
              },
            },
          },
        },
      });
      if (data) {
        cms.alerts.success("Changes saved");
      } else {
        cms.alerts.error("Error saving changes");
      }
    },
    fields: [],
  };

  const [home, form] = useForm(formConfig);
  usePlugin(form);

  return (
    <div>
      <Head>
        <title>Strapi</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <InlineForm form={form} initialStatus="active">
        <Main>
          <Hero {...home.Hero} />
          <ProductGrid>
            {home.products?.map((product) => (
              <Product key={product.id} {...product} />
            ))}
          </ProductGrid>
        </Main>
      </InlineForm>
    </div>
  );
}

export async function getStaticProps({ params, preview, previewData }) {
  const apolloClient = initializeApollo();

  const { data } = await apolloClient.query({
    query: homePageQuery,
  });

  if (preview) {
    return addApolloState(apolloClient, {
      props: { home: data.homePage, preview, ...previewData },
      revalidate: 1,
    });
  }

  return addApolloState(apolloClient, {
    props: { home: data.homePage, preview: false },
    revalidate: 1,
  });
}

const Main = styled.main`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ProductGrid = styled.div`
  display: grid;
  grid-template-columns: repeat(1, 1fr);
  grid-gap: 1rem;
  max-width: 1150px;
  width: fit-content;
  padding: 3rem 1rem;

  @media screen and (min-width: 600px) {
    grid-template-columns: repeat(2, 1fr);
  }
`;

const homePageQuery = gql`
  query {
    homePage {
      id
      Hero {
        id
        heroImage {
          id
          url
        }
        heroTitle
        heroTagline
        CTA {
          ctaText
          ctaLink
        }
      }
      products {
        id
        productImage {
          id
          url
        }
        productTitle
        productDescription
        productPrice
      }
    }
  }
`;

const updateHomePageMutation = gql`
  mutation UpdateHomePage($input: updateHomePageInput) {
    updateHomePage(input: $input) {
      homePage {
        Hero {
          heroImage {
            url
          }
          heroTitle
          heroTagline
          CTA {
            ctaText
            ctaLink
          }
        }
      }
    }
  }
`;

import { useMemo } from "react";
import { createGlobalStyle } from "styled-components";
import {
  StrapiMediaStore,
  StrapiProvider,
  StrapiClient,
} from "react-tinacms-strapi";
import { TinaCMS, TinaProvider } from "tinacms";
import { ApolloProvider } from "@apollo/client";
import { useApollo } from "../utils/apolloClient";

import EditButton from "../components/editButton";
import { getStrapiURL } from "../utils/api";

export default function MyApp({ Component, pageProps }) {
  const cms = useMemo(
    () =>
      new TinaCMS({
        enabled: pageProps.preview,
        toolbar: pageProps.preview,
        sidebar: pageProps.preview,
        apis: {
          strapi: new StrapiClient(getStrapiURL()),
        },
        media: new StrapiMediaStore(getStrapiURL()),
      }),
    []
  );
  const apolloClient = useApollo(pageProps);

  return (
    <TinaProvider cms={cms}>
      <StrapiProvider onLogin={enterEditMode} onLogout={exitEditMode}>
        <ApolloProvider client={apolloClient}>
          <Global />
          <EditButton />
          <Component {...pageProps} />
        </ApolloProvider>
      </StrapiProvider>
    </TinaProvider>
  );
}

const enterEditMode = () => {
  return fetch(`/api/preview`).then(() => {
    window.location.href = window.location.pathname;
  });
};

const exitEditMode = () => {
  return fetch(`/api/reset-preview`).then(() => {
    window.location.reload();
  });
};

const Global = createGlobalStyle`
  html,
  body {
    padding: 0;
    margin: 0;
    font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
      Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
  }

  a {
    color: inherit;
    text-decoration: none;
  }

  * {
    box-sizing: border-box;
  }
`;

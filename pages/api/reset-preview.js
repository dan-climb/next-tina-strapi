export default function resetPreviewHandler(req, res) {
  res.clearPreviewData();
  res.status(200).end();
}

import Head from "next/head";
import styled from "styled-components";
import { useCMS, useForm, usePlugin } from "tinacms";
import { InlineForm } from "react-tinacms-inline";
import { Product } from "../../components/product";
import { fetchGraphql } from "../../utils/api";

export default function ProductPage({ product: initialProduct, preview }) {
  const cms = useCMS();
  const formConfig = {
    id: initialProduct.id,
    label: "Product",
    initialValues: initialProduct,
    onSubmit: async (values) => {
      const { data } = await cms.api.strapi.fetchGraphql(
        updateProductMutation,
        {
          input: {
            where: { id: values.id },
            data: {
              productImage: values.productImage.id,
              productTitle: values.productTitle,
              productDescription: values.productDescription,
              productPrice: values.productPrice,
              categories: values.categories.map(({ id }) => id),
            },
          },
        }
      );
      if (data) {
        cms.alerts.success("Changes saved");
      } else {
        cms.alerts.error("Error saving changes");
      }
    },
    fields: [
      {
        label: "Image",
        name: "productImage.id",
        component: "image",
        parse: (media) => media.id,
        uploadDir: () => "/",
      },
      { label: "Title", name: "productTitle", component: "text" },
      { label: "Description", name: "productDescription", component: "text" },
      { label: "Price", name: "productPrice", component: "number" },
    ],
  };

  const [product, form] = useForm(formConfig);
  usePlugin(form);

  return (
    <div>
      <Head>
        <title>Strapi</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <InlineForm form={form} initialStatus="active">
        <Main>
          <Product {...product} />
        </Main>
      </InlineForm>
    </div>
  );
}

export async function getStaticProps({ params, preview, previewData }) {
  const { product } = await fetchGraphql(getProductQuery, { id: params.id });

  if (preview) {
    return { props: { product, preview, ...previewData } };
  }

  return { props: { product, preview: false } };
}

export async function getStaticPaths() {
  const { products } = await fetchGraphql(allProductIdsQuery);

  return {
    paths: products.map((product) => {
      return {
        params: {
          id: product.id,
        },
      };
    }),
    fallback: false,
  };
}

const Main = styled.main`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const getProductQuery = `
  query($id: ID!) {
    product(id: $id) {
      id
      productImage {
        id
        url
      }
      productTitle
      productDescription
      productPrice
      categories {
        id
        categoryName
      }
    }
  }
`;

const allProductIdsQuery = `
  query {
    products {
      id
    }
  }
`;

const updateProductMutation = `
  mutation UpdateProduct($input: updateProductInput) {
    updateProduct(input: $input) {
      product {
        id
        productImage {
          id
          url
        }
        productTitle
        productDescription
        productPrice
        categories {
          id
          categoryName
        }
      }
    }
  }
`;
